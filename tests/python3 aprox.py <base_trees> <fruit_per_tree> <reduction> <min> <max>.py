#!/usr/bin/env python3

'''
Cálculo del número óptimo de árboles.
'''

import sys


def compute_production(trees, base_trees, reduction, fruit_per_tree):
    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production


def compute_all(base_trees, reduction, fruit_per_tree, min_trees, max_trees):
    productions = []
    for trees in range(min_trees, max_trees + 1):
        production = compute_production(trees, base_trees, reduction, fruit_per_tree)
        productions.append((trees, production))
    return productions


def read_arguments():
    if len(sys.argv) != 6:
        print("Uso: script.py base_trees fruit_per_tree reduction min max")
        sys.exit(1)
    base_trees = int(sys.argv[1])
    fruit_per_tree = float(sys.argv[2])
    reduction = float(sys.argv[3])
    min_trees = int(sys.argv[4])
    max_trees = int(sys.argv[5])
    return base_trees, fruit_per_tree, reduction, min_trees, max_trees


def main():
    base_trees, fruit_per_tree, reduction, min_trees, max_trees = read_arguments()
    productions = compute_all(base_trees, reduction, fruit_per_tree, min_trees, max_trees)

    best_trees, best_production = max(productions, key=lambda x: x[1])

    print(f"Best production: {best_production}, for {best_trees} trees")


if __name__ == '__main__':
    main()
